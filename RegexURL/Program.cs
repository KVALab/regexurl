﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;


namespace RegexURL
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            using (WebClient webClient = new WebClient() { Credentials = CredentialCache.DefaultNetworkCredentials })
            {
                MenuGetURL(webClient);
            }
        }


        /// <summary>
        /// Основная функция поиска
        /// </summary>
        static void MenuGetURL(WebClient webClient)
        {
            while (true)
            {
                System.Console.WriteLine("\nВведите URL страницы для получения изображений или E(для выхода): ");
                var url = System.Console.ReadLine();

                if (string.IsNullOrEmpty(url))
                {
                    Console.WriteLine($"Ссылка для поиска не может быть пустой!");
                }
                else
                {
                    //Выход из программы
                    if (ExitProgram(url))
                        break;

                    //Получить данные в виде html со страницы
                    string rawhtml = GetURLData(webClient, url);
                    if (string.IsNullOrEmpty(rawhtml))
                    {
                        Console.WriteLine($"Данные страницы по указанной ссылке {url} не найдены!");
                    }
                    else
                    {
                        //Собрать список ссылок на изображения через Regex
                        List<string> listPictRef = FindPictureReference(rawhtml);
                        if (listPictRef.Count == 0)
                        {
                            Console.WriteLine($"Ссылки изображений на данной странице {url} не найдены!");
                        }
                        else
                        {
                            UploadPicture(webClient, url, listPictRef);
                        }
                    }
                }

            }
        }


        private static void UploadPicture(WebClient webClient, string url, List<string> listPictRef)
        {
            //Получить имя файла через Regex
            Regex regFileName = new Regex(@"([\w|\-|\.]+)(?!\/)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            //Проверка https в ссылке
            Regex regCheckHttp = new Regex(@"https?\:", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            //Получить домен для относительных ссылок
            Regex regGetDomain = new Regex(@"https?\:\/+[\w|\.]+", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            string domain = regGetDomain.Match(url).Value;

            string urlPicture;
            int countUploadPict = 0;
            foreach (string item in listPictRef)
            {
                urlPicture = item;
                if (!urlPicture.Contains("//")) //Добавить домен, путь относительный
                    urlPicture = domain + urlPicture;

                if (!regCheckHttp.IsMatch(urlPicture))
                    urlPicture = "https:" + urlPicture;

                try
                {
                    string pictureName = regFileName.Match(urlPicture).Groups[0].Value;
                    webClient.DownloadFile(urlPicture, $"foto\\{pictureName}");
                    Console.WriteLine(urlPicture);
                    countUploadPict++;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Ошибка загрузки файла по ссылке:{urlPicture}\r\n{e.Message}");
                    countUploadPict--;
                }
            }

            Console.WriteLine($"Количество выгруженных файлов: {countUploadPict}");
        }


        //Найти все ссылки изображений на странице
        static List<string> FindPictureReference(string rawhtml)
        {
            Regex regPict = new Regex(@"<img(?=.*href)?|(?=.*src)?("")(\S+\.(jpg|png|bmp|tiff))(\1)");

            var listRefsPict = regPict.Matches(rawhtml)
                                    .Cast<Match>()
                                    .Select(s => s.Groups[2].Value)
                                    .Where(w => w.Contains('/'))
                                    .Distinct()
                                    .ToList();
            return listRefsPict;
        }


        //Проверка выхода из программы по введенному символу E
        static bool ExitProgram(string url)
        {
            return (url.Length == 1 &&
                    new List<char>() { 'E', 'Е' }.Contains(url.ToUpper()[0]));
        }


        /// <summary>
        /// Получить данные из сети по переданной ссылке
        /// </summary>
        /// <param name="url">ссылка для загрузки</param>
        /// <returns>строка содержащая html</returns>
        static string GetURLData(WebClient webClient, string url)
        {
            string html = string.Empty;
            try
            {
                html = webClient.DownloadString(url);
            }
            catch (WebException we)
            {
                Console.WriteLine(we.Message);
            }

            return html;
        }
    }
}
